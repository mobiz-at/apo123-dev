<?php


define('MAGENTO', realpath(dirname(__FILE__)."/.."));
require_once MAGENTO . '/app/Mage.php';

umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);


$file = fopen(MAGENTO . '/var/import/lagerstand.csv', 'r');
while (($line = fgetcsv($file,0,";")) !== FALSE) {

  $data[$line[0]] = $line[1];


}//end while
fclose($file);

$resource = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');
$table = $resource->getTableName('cataloginventory/stock_item');
Mage::log($table);
$query = "UPDATE $table SET qty=0";
$writeConnection->query($query);
$query = "UPDATE $table SET is_in_stock=0";
$writeConnection->query($query);




$coll = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('entity_id')
->addAttributeToFilter( 'sku', array( 'in' => array_keys($data) ))
->load();
Mage::log($coll->getsize());


$prods = $coll->toArray();
#Mage::log($prods);

foreach($prods as $pid=>$arr){

   Mage::log($pid);
  $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($pid);
  #if(isset($data[$pid])){
    $stockItem->setData("qty", $data[$arr['sku']]);
    if($data[$arr['sku']]){
        $stockItem->setData("is_in_stock", 1);
    }
  #}else{
    #$stockItem->setData("qty", 0);
    #$stockItem->setData("is_in_stock", 0);
  #}

  $stockItem->save();
  unset($stockItem);

}