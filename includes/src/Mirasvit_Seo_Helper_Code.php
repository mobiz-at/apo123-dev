<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Advanced SEO Suite
 * @version   1.0.3
 * @revision  279
 * @copyright Copyright (C) 2013 Mirasvit (http://mirasvit.com/)
 */


class Mirasvit_Seo_Helper_Code extends Mirasvit_MstCore_Helper_Code
{
    protected $k = "OMJ0GV5ARK";
    protected $s = "SEO";
    protected $o = "1486";
    protected $v = "1.0.3";
    protected $r = "279";
    protected $p = "MCore/1.0.6-69|SEO/1.0.3-210";
}
