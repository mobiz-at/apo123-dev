<?php


class SM_XPayment2_Block_Info_Bacs extends Mage_Payment_Block_Info
{

    protected $_instructions;

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('payment/info/bacs.phtml');
    }
	
    public function getInstructions()
    {
        if (is_null($this->_instructions)) {
            $this->_convertAdditionalData();
        }
        return $this->_instructions;
    }

    protected function _convertAdditionalData()
    {
        $details = @unserialize($this->getInfo()->getAdditionalData());
        if (is_array($details)) {
            $this->_instructions = isset($details['instructions']) ? (string) $details['instructions'] : '';
        } else {
            $this->_instructions = '';
        }
        return $this;
    }

}
