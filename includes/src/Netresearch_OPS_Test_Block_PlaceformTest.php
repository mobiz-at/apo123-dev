<?php
class Netresearch_OPS_Test_Block_PlaceformTest extends EcomDev_PHPUnit_Test_Case
{
    public function testGetFormAction()
    {
        $block = Mage::app()->getLayout()->getBlockSingleton('ops/placeform');
        $blockMock = $this->getBlockMock('ops/placeform', array('getQuestion'));
        $blockMock->expects($this->any())
            ->method('getQuestion')
            ->will($this->returnValue('How much is the fish?'));

        $action = $blockMock->getFormAction();
        $this->assertEquals(Mage::getUrl('*/*/*', array('_secure' => Mage::app()->getFrontController()->getRequest()->isSecure())), $action);

        // check explicitly for https
        $_SERVER['HTTPS'] = 'on';
        $action = $blockMock->getFormAction();
        $this->assertEquals(Mage::getUrl('*/*/*', array('_secure' =>true)), $action);

        $blockMock = $this->getBlockMock('ops/placeform', array('getQuestion'));
        $blockMock->expects($this->any())
            ->method('getQuestion')
            ->will($this->returnValue(null));

        $action = $blockMock->getFormAction();
        $this->assertEquals($blockMock->getConfig()->getFrontendGatewayPath(), $action);
    }
}