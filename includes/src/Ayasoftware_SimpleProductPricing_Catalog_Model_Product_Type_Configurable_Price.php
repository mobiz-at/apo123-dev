<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Product type price model
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Ayasoftware_SimpleProductPricing_Catalog_Model_Product_Type_Configurable_Price extends Mage_Catalog_Model_Product_Type_Price
{

	/**
	 * Check if string s1 contains string s2
	 * @param $s1
	 * @param $s2
	 * @return boolean: true is yes
	 */
	protected function strContains ($s1, $s2)
	{
		$pos = strpos(strtolower($s1), strtolower($s2));
		if ($pos !== false) {
			return true;
		}
		return false;
	}
	/**
	 * Get product final price
	 *
	 * @param   double $qty
	 * @param   Mage_Catalog_Model_Product $product
	 * @return  double
	 */
	public function getFinalPrice ($qty = null, $product)
	{
		$useSimpleProductPricing = false;
		if(!Mage::getStoreConfig('spp/setting/enableModule')){
			return parent::getFinalPrice($qty, $product);
		}
		$session = Mage::getSingleton('checkout/session');
		$currentUrl = Mage::helper('core/url')->getCurrentUrl();
		$tierPrice = 0;
		$simplePrice = 0;
		$paymentUrls = array(
		1 => "onepagecheckout",
		2 => "formPayment/go",
		3 => "aitcheckout",
		4 => "onestepcheckout",
		5 => "checkout/cart",
		6 => "checkout/onepage",
		7 => "paypal/express",
		8 => "checkout/multishipping",
		9 => "authorizenet/directpost_payment",
		10 => "firecheckout"
		);
		foreach ($paymentUrls as  $key => $paymentUrl){
			if ($this->strContains($currentUrl, $paymentUrl)  ){
				$useSimpleProductPricing = true;
				break;
			}
		}
		if($useSimpleProductPricing) {
			if (is_null($qty) && ! is_null($product->getCalculatedFinalPrice())) {
				return $product->getCalculatedFinalPrice();
			}
			$catalog = Mage::getModel('catalog/product'); // for some reason i will need to reload product to get attributes
			$_config = $catalog->load($product->getId());
			if ($_config->getUseStandardConfig()) {
				$simplePrice =  parent::getFinalPrice($qty, $product);
			}
			$productOptions = $product->getTypeInstance(true)->getOrderOptions($product);
			$simple =  $catalog->load($product->getIdBySku($productOptions['simple_sku']));
			$simplePrice	= $simple->getFinalPrice();
			if($simple->getCustomerGroupId()){
				$simplePrice = $simple->getGroupPrice();
			}
			if ($this->canApplyTierPrice($simple, $qty)) {
				$simplePrice  = $simple->getTierPrice($qty);
			}
				
			if ($simple->special_price) {
				$simplePrice = min ($simple->getFinalPrice(),$simplePrice);
			}

			/*
			 // BOF super attributes configuration
			 $finalPrice = parent::getFinalPrice($qty, $product);
			 $beforeSelections =  parent::getFinalPrice($qty, $product);
			 $product->getTypeInstance(true)->setStoreFilter($product->getStore(), $product);
			 $attributes = $product->getTypeInstance(true)->getConfigurableAttributes($product);
			 $selectedAttributes = array();
			 if ($product->getCustomOption('attributes')) {
			 $selectedAttributes = unserialize($product->getCustomOption('attributes')->getValue());
			 }
			 $basePrice = $simplePrice;
			 foreach ($attributes as $attribute) {
			 $attributeId = $attribute->getProductAttribute()->getId();
			 $value = $this->_getValueByIndex($attribute->getPrices() ? $attribute->getPrices() : array(), isset($selectedAttributes[$attributeId]) ? $selectedAttributes[$attributeId] : null);
			 if ($value) {
				if ($value['pricing_value'] != 0) {
				$finalPrice += $this->_calcSelectionPrice($value, $basePrice);
				}
				}
				}
				$super_attributes_price =  $finalPrice - $beforeSelections;
				// EOF super attributes configuration

				*/
			if($this->applyRulesToProduct($simple)){
				$rulePrice = $this->applyRulesToProduct($simple);
				if($this->applyOptionsPrice($product,$simplePrice)){
					$rulePrice = $this->applyOptionsPrice($product,$rulePrice);
					$simplePrice = $this->applyOptionsPrice($product,$simplePrice);
				}
				$product->setFinalPrice(min($simplePrice,$rulePrice) );
				return min($simplePrice,$rulePrice);
			} else {
				if($this->applyOptionsPrice($product,$simplePrice)){
					$simplePrice = $this->applyOptionsPrice($product,$simplePrice);
				}
				$product->setFinalPrice($simplePrice);
				return $simplePrice;
			}
		} else {
			return $this->getDefaultPrice($qty = null, $product);
		}
	}
	// Use Magento Default getFinalPrice()
	public function getDefaultPrice ($qty = null, $product){
			
		if (is_null($qty) && ! is_null($product->getCalculatedFinalPrice())) {
			return $product->getCalculatedFinalPrice();
		}
		$finalPrice = parent::getFinalPrice($qty, $product);
		$product->getTypeInstance(true)->setStoreFilter($product->getStore(), $product);
		$attributes = $product->getTypeInstance(true)->getConfigurableAttributes($product);
		$selectedAttributes = array();
		if ($product->getCustomOption('attributes')) {
			$selectedAttributes = unserialize($product->getCustomOption('attributes')->getValue());
		}
		$basePrice = $finalPrice;
		foreach ($attributes as $attribute) {
			$attributeId = $attribute->getProductAttribute()->getId();
			$value = $this->_getValueByIndex($attribute->getPrices() ? $attribute->getPrices() : array(), isset($selectedAttributes[$attributeId]) ? $selectedAttributes[$attributeId] : null);
			if ($value) {
				if ($value['pricing_value'] != 0) {
					$finalPrice += $this->_calcSelectionPrice($value, $basePrice);
				}
			}
		}

		if(Mage::app()->getRequest()->getControllerName() == 'product'){
			$prices = Mage::helper('spp')->getCheapestChildPrice($product);
			$product->setFinalPrice($prices['Min']['finalPrice']);
			$product->setPrice($prices['Min']['price']);
			return max(0,$prices['Min']['finalPrice']);
		} else {
			$product->setFinalPrice($finalPrice);
			return max(0, $product->getData('final_price'));
		}

	}

	protected function _calcSelectionPrice ($priceInfo, $productPrice)
	{
		if ($priceInfo['is_percent']) {
			$ratio = $priceInfo['pricing_value'] / 100;
			$price = $productPrice * $ratio;
		} else {
			$price = $priceInfo['pricing_value'];
		}
		return $price;
	}
	protected function _getValueByIndex ($values, $index)
	{
		foreach ($values as $value) {
			if ($value['value_index'] == $index) {
				return $value;
			}
		}
		return false;
	}

	protected function applyOptionsPrice($product, $finalPrice)
	{
		if ($optionIds = $product->getCustomOption('option_ids')) {
			$basePrice = $finalPrice;
			foreach (explode(',', $optionIds->getValue()) as $optionId) {
				if ($option = $product->getOptionById($optionId)) {

					$confItemOption = $product->getCustomOption('option_'.$option->getId());
					$group = $option->groupFactory($option->getType())
					->setOption($option)
					->setConfigurationItemOption($confItemOption);

					$finalPrice += $group->getOptionPrice($confItemOption->getValue(), $basePrice);
				}
			}
		}
		return $finalPrice;
	}
	protected function canApplyTierPrice($product, $qty){
		$tierPrice  = $product->getTierPrice($qty);
		if(empty($tierPrice)){
			return false;
		}
		$price = $product->getPrice();
		if ($tierPrice != $price ){
			return true;
		} else {
			return false;
		}
	}
	/**
	 *
	 * Apply Catalog Rules...
	 * @param  int|Mage_Catalog_Model_Product $product
	 */

	public function applyRulesToProduct($product)
	{
		$rule = Mage::getModel("catalogrule/rule");
		return $rule->calcProductPriceRule($product,$product->getPrice());
	}

	//Force tier pricing to be empty for configurable products:
	public function getTierPrice($qty=null, $product)
	{
		return array();
	}


}
