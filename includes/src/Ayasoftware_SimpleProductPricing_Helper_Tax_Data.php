<?php

class Ayasoftware_SimpleProductPricing_Helper_Tax_Data extends Mage_Tax_Helper_Data {

    public function getPrice($product, $price, $includingTax = null, $shippingAddress = null, $billingAddress = null,
    		$ctc = null, $store = null, $priceIncludesTax = null, $roundPrice = true)
    {
    	
    	$price =  parent::getPrice($product, $price, $includingTax , $shippingAddress , $billingAddress,
    		$ctc, $store, $priceIncludesTax, $roundPrice);
    	
    	if (Mage::app()->getRequest()->getControllerName() != 'product' && $product->getTypeId() == 'configurable' && Mage::getStoreConfig('spp/setting/enableModule')) {
    		$prices = Mage::helper('spp')->getCheapestChildPrice($product);
    	    $product->setPrice($prices['Min']['price']);
    		$product->setFinalPrice($prices['Min']['finalPrice']);
    	}
    	
    	return $price;
    	
    	
    	
    }
}

