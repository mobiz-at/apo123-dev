<?php

class WP_AdvancedSlider_Block_Adminhtml_Sliders_Edit_Tab_General_Displayon
    extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('advancedslider/form.phtml');
    }

    protected function _getSlider()
    {
        return Mage::registry('slider');
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('advancedslider_slider_display_on', array());
        $fieldset->setRenderer(Mage::getBlockSingleton('advancedslider/adminhtml_widget_form_renderer_fieldset'));

        $fieldset->addType('categories_chooser','WP_AdvancedSlider_Block_Adminhtml_Widget_Form_Element_Categorychooser');

        $fieldset->addField('categories', 'categories_chooser', array(
            'name'      => 'categories',
            'label'     => $this->__('Categories'),
            'note'      => $this->__('The slider will be displayed after a title of a category.'),
            'required'  => false,
            'value'     => $this->_getSlider()->getCategories(),
        ));
        return parent::_prepareForm();
    }
}
