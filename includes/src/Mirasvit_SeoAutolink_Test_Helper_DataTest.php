<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Advanced SEO Suite
 * @version   1.0.3
 * @revision  279
 * @copyright Copyright (C) 2013 Mirasvit (http://mirasvit.com/)
 */



class Mirasvit_SEO_Test_Helper_DataTest extends PHPUnit_Framework_TestCase 
{
    public function setUp() {
        parent::setUp();
        $this->parseHelper = Mage::helper('seoautolink');
  //       //we load product from collection, because we need to load its attributes
  //       $collection = Mage::getModel('catalog/product')->getCollection()
  //       				->addFieldToFilter('entity_id', 166)
  //       				->addAttributeToSelect('*')
  //       				;
  //       $product = $collection->getFirstItem(); //http://devstore.dva/electronics/cell-phones/htc-touch-diamond.html
  //   	$category = Mage::getModel('catalog/category')->load(8); //http://devstore.dva/electronics/cell-phones.html
  //   	$store = Mage::getModel('core/store')->load(1);

  //   	$product->setName("HTC Touch Diamond [ru, uk] {ru, uk}");

  //   	$this->objects = array(
  //   		'product'=>$product,
  //   		'category'=>$category,
  //   		'store'=>$store,
		// );

    }

	/**
	* @dataProvider parseProvider 
	*/
    public function testParse($text, $links, $expectedResult) {
    	$result = $this->parseHelper->_addLinks($text, $links);

        $this->assertequals($expectedResult, $result);
    }

    public function parseProvider()
    {

        $link1 = new Varien_Object(array(
            'keyword' => 'link1',
            'url' => 'http://link1.com',
            ));
        $link2 = new Varien_Object(array(
            'keyword' => 'link2',
            'url' => 'http://link2.com',
            ));
        $link3 = new Varien_Object(array(
            'keyword' => 'link2 link3',
            'url' => 'http://link3.com',
            ));
        return array(
          array('link1 link2', array($link1, $link2, $link3), "<a href='http://link1.com' >link1</a> <a href='http://link2.com' >link2</a>"),
          array('link1 link2 link3', array($link1, $link2, $link3), "<a href='http://link1.com' >link1</a> <a href='http://link2.com' >link2</a> link3"),
          array("<a href='http://link1.com' >link1 aaaa</a>", array($link1, $link3, $link2), "<a href='http://link1.com' >link1 aaaa</a>"),
          array('link2 link3', array($link3, $link2), "<a href='http://link3.com' >link2 link3</a>"),
          array('Link2', array($link3, $link2), "<a href='http://link2.com' >Link2</a>"),
         );
    }    
 
}
