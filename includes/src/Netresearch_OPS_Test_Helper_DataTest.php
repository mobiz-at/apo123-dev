<?php
class Netresearch_OPS_Test_Helper_DataTest extends EcomDev_PHPUnit_Test_Case_Controller
{
    protected $helper;
    protected $store;
 
    public function setUp()
    {
        parent::setup();
        $this->helper = Mage::helper('ops');
        $this->store  = Mage::app()->getStore(0)->load(0);
    }
    
    /**
     * @test
     */
    public function getModuleVersionString()
    {
        $path = 'modules/Netresearch_OPS/version';

        Mage::getConfig()->setNode('modules/Netresearch_OPS/version', '120301');
        $this->assertSame('OGmg120301', $this->helper->getModuleVersionString());

        Mage::getConfig()->setNode('modules/Netresearch_OPS/version', '120612');
        $this->assertSame('OGmg120612', $this->helper->getModuleVersionString());

        $this->store->resetConfig();
    }
    
    public function testCheckIfUserIsRegistering()
    {
        $quote = new Varien_Object();
        $quote->setCheckoutMethod(Mage_Sales_Model_Quote::CHECKOUT_METHOD_REGISTER);
        $sessionMock = $this->getModelMock('checkout/session', array('getQuote'));
        $sessionMock->expects($this->any())
            ->method('getQuote')
            ->will($this->returnValue($quote));
        $this->replaceByMock('model', 'checkout/session', $sessionMock);
    
        
        $this->assertTrue(Mage::helper('ops/data')->checkIfUserIsRegistering());
        
        $quote->setCheckoutMethod(Mage_Sales_Model_Quote::CHECKOUT_METHOD_LOGIN_IN);
        $this->assertTrue(Mage::helper('ops/data')->checkIfUserIsRegistering());
        
        $quote->setCheckoutMethod(Mage_Sales_Model_Quote::CHECKOUT_METHOD_GUEST);
        $this->assertFalse(Mage::helper('ops/data')->checkIfUserIsRegistering());
        
    }
    
    public function testCheckIfUserIsNotRegistering()
    {
        $quote = new Varien_Object();
        $quote->setCheckoutMethod(Mage_Sales_Model_Quote::CHECKOUT_METHOD_REGISTER);
        $sessionMock = $this->getModelMock('checkout/session', array('getQuote'));
        $sessionMock->expects($this->any())
            ->method('getQuote')
            ->will($this->returnValue($quote));
        $this->replaceByMock('model', 'checkout/session', $sessionMock);
        
        $this->assertTrue(Mage::helper('ops/data')->checkIfUserIsNotRegistering());
        
        $quote->setCheckoutMethod(Mage_Sales_Model_Quote::CHECKOUT_METHOD_LOGIN_IN);
        $this->assertFalse(Mage::helper('ops/data')->checkIfUserIsNotRegistering());
        
        $quote->setCheckoutMethod(Mage_Sales_Model_Quote::CHECKOUT_METHOD_GUEST);
        $this->assertFalse(Mage::helper('ops/data')->checkIfUserIsNotRegistering());
    }
}

