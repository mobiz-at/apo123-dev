<?php
class Apo123_Custompricing_Model_Observer
{
   public function finalPriceFormula($_price, $_aepneu, $_tax_class_id)
	{
		
		if ($_tax_class_id == 6) :
			$_vat_factor = 1.2;
		elseif ($_tax_class_id == 5) :
			$_vat_factor = 1.1;
		endif;
		
		$_preliminary_special_price = ($_aepneu + ((($_price / $_vat_factor) - $_aepneu) * 0.5)) * 1.25;
		$_savings = (1 - ($_preliminary_special_price / $_price)) * 100;
		
		if ($_preliminary_special_price < ($_price * 0.95) ) :
			$finalPrice = $_preliminary_special_price;
		else:
			$finalPrice = $_price * 0.95;
		endif;
		
		//$finalPrice = 999;
		return $finalPrice;
	}
   
   public function calculateFinalPrice($observer)
   {
	   
       $_product = $observer->getEvent()->getProduct();
       
        // logic goes here...
        //$_price = $_product->getPrice();
		//$_aepneu = $_product->getAepneu();
		//$_tax_class_id = $_product->getTaxClassId();
		
		// testing values, comment after test.
		//$_price = 7.05;
		//$_aepneu = 4.07;
		//$_tax_class_id = 6;
       
       //$this->finalPriceFormula($_product->getPrice(), $_product->getAepneu(), $_product->getTaxClassId());
		
       // set the final price
       if($_product->getData('type_id')=='configurable'):
			// set the configurable product's final price
			$_product->setFinalPrice($this->finalPriceFormula( $_product->getPrice(), $_product->getAepneu(), $_product->getTaxClassId() )); 
			// fetch the child products of configurable product
			$_conf_products = $_product->getTypeInstance(true)->getUsedProducts(null, $_product);
			foreach ($_conf_products as $_conf_product){
				// set the attributed products final price
				$_conf_product->setFinalPrice($this->finalPriceFormula( $_conf_product->getPrice(), $_conf_product->getAepneu(), $_conf_product->getTaxClassId() ));
			}
       else:
			// simple product
			$_product->setFinalPrice($this->finalPriceFormula($_product->getPrice(), $_product->getAepneu(), $_product->getTaxClassId())); 
	   endif;
 
     return $this;
   }
   
   public function collectionFinalPrice($observer)
   {
       $collection = $observer->getEvent()->getCollection();
       
       foreach($collection as $_product){
		//logic goes here...
        $_price = $_product->getPrice();
		$_aepneu = $_product->getData("aepneu");
		//$productId  = $_product->getId();
		//$_product2 = Mage::getModel('catalog/product')->load($productId);
		//$_tax_class_id = $_product2->getData("tax_class_id");
		$_tax_class_id = $_product->getData("tax_class_id");
		
		//$_price = 7.05;
		//$_aepneu = 4.07;
		//$_tax_class_id = 6;
		
       	if ($_tax_class_id == 6) :
			$_vat_factor = 1.2;
		elseif ($_tax_class_id == 5) :
			$_vat_factor = 1.1;
		endif;
		$_preliminary_special_price = ($_aepneu + ((($_price / $_vat_factor) - $_aepneu) * 0.5)) * 1.25;
		$_savings = (1 - ($_preliminary_special_price / $_price)) * 100;
		if ($_preliminary_special_price < ($_price * 0.95) ) :
			$finalPrice = $_preliminary_special_price;
		else:
			$finalPrice = $_price * 0.95;
		endif; 
		
		 //set it
		 //$_product->setMinimalPrice($finalPrice)
				  //->setPrice($_price)
				  //->setFinalPrice($finalPrice);
		 $_product->setMinimalPrice($_aepneu)
				  ->setFinalPrice($_aepneu);
       }
       return $this;
   }
   
}
