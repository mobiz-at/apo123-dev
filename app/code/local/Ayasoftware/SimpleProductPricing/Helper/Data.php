<?php

class Ayasoftware_SimpleProductPricing_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getCheapestChildPrice($product) {
        $productIds = array();
        $saveCache = false;
        $cache = Mage::app()->getCache();
        $cacheKey = $product->getId();

        if (intval(Mage::getStoreConfig('spp/setting/spp_cache_lifetime')) > 0) {
            if ($cache->load("prodIds_" . $cacheKey)) {
                $cached_value = unserialize($cache->load("prodIds_" . $cacheKey));
                return $cached_value;
            } else {
                $saveCache = true;
            }
        }

        if ($product->getTypeId() != 'configurable') {
            return;
        }

        $childProducts = $product->getTypeInstance(true)->getUsedProductCollection($product);
        $childProducts->addAttributeToSelect(array('msrp', 'price', 'special_price', 'status', 'special_from_date', 'special_to_date'));
        $showOutOfStock = Mage::getStoreConfig('spp/setting/show');
		foreach ($childProducts as $childProduct) {
            if (!$childProduct->isSalable()) {
            	 if(!$showOutOfStock){
            	 	continue;
            	 }
            }
            $finalPrice = $childProduct->getFinalPrice();
            if ($childProduct->getTierPrice()) {
                $tprices = array();
                foreach ($tierprices = $childProduct->getTierPrice() as $tierprice) {
                    $tprices[] = $tierprice['price'];
                }
            }
            if (!empty($tprices)) {
                $finalPrice = min($tprices);
            }
            Mage::dispatchEvent('catalog_product_get_final_price', array('product' => $childProduct, 'qty' => 1));
            $productIds[$childProduct->getId()] = array("finalPrice" => $childProduct->getFinalPrice(), "price" => $childProduct->getPrice());
        }
        $productCheapestId = array_search(min($productIds), $productIds);
        $productExpensiveId = array_search(max($productIds), $productIds);
        $prices = array();
        $prices["Min"] = array("finalPrice" => $productIds[$productCheapestId]['finalPrice'], 'price' => $productIds[$productCheapestId]['price']);
        $prices["Max"] = array("finalPrice" => $productIds[$productExpensiveId]['finalPrice'], 'price' => $productIds[$productExpensiveId]['price']);

        if (intval(Mage::getStoreConfig('spp/setting/spp_cache_lifetime')) > 0 && $saveCache) {
            $cache->save(serialize($prices), "prodIds_" . $cacheKey, array("prodIds_cache"), intval(Mage::getStoreConfig('spp/setting/spp_cache_lifetime')));
        }
        return $prices;
    }

}

