<?php


class SM_XPayment2_Model_Method_XPayment2 extends Mage_Payment_Model_Method_Abstract
{

    protected $_code  = 'XPayment2';
    protected $_formBlockType = 'XPayment2/form_bacs';
    protected $_infoBlockType = 'XPayment2/info_bacs';
	
	protected $_isInitializeNeeded      = true;
	protected $_canUseInternal          = true;
	protected $_canUseCheckout          = false;
	protected $_canUseForMultishipping  = false;
	
    public function assignData($data)
    {
        $details = array();
		
        if ($this->getInstructions()) {
            $details['instructions'] = $this->getInstructions();
        }
        if (!empty($details)) {
            $this->getInfoInstance()->setAdditionalData(serialize($details));
        }
        return $this;
    }

    public function getInstructions()
    {
        return $this->getConfigData('instructions');
    }

}
