<?php

class SM_XPayment2_Block_Form_Bacs extends Mage_Payment_Block_Form
{

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('payment/form/bacs.phtml');
    }

}
