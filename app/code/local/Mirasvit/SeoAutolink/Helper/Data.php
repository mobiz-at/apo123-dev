<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Advanced SEO Suite
 * @version   1.0.3
 * @revision  279
 * @copyright Copyright (C) 2013 Mirasvit (http://mirasvit.com/)
 */


class Mirasvit_SeoAutolink_Helper_Data extends Mage_Core_Helper_Abstract
{
	private $links;

	public function getLinks($text) {
		Varien_Profiler::start('seoautolink_getLinks');
		$links = Mage::getModel('seoautolink/link')->getCollection()
					->addActiveFilter()
					->addStoreFilter(Mage::app()->getStore())
					;
		$links->getSelect()
			->where("lower('".addslashes($text)."') LIKE CONCAT('%', lower(keyword), '%')")
			->order( array('LENGTH(main_table.keyword) desc') ); //we need to replace long keywords firstly
		Varien_Profiler::stop('seoautolink_getLinks');
		return $links;
	}

	public function addLinks($text) {
		return $this->_addLinks($text, $this->getLinks($text));
	}
	
	public function _addLinks($text, $links) {
		Varien_Profiler::start('seoautolink_addLinks');
		foreach ($links  as $link) {
			$nofollow = $link->getIsNofollow?'rel=\'nofollow\'':'';

			$target = $link->getUrlTarget()?" target='{$link->getUrlTarget()}' ":"";			
			$html = "<a href='{$link->getUrl()}'$target $nofollow>----->{$link->getKeyword()}</a>";//-----> Ð½Ð°Ð¼ Ð½ÑÐ¶Ð½Ð¾, ÑÑÐ¾Ð± Ð² Ð´Ð°Ð»ÑÐ½ÐµÑÐµÐ¼ Ð¿Ð¾Ð´Ð¼ÐµÐ½Ð¸ÑÑ ÐºÐ»ÑÑÐµÐ²Ð¾Ðµ ÑÐ»Ð¾Ð²Ð¾, Ð½Ð° ÑÑÐ¾ Ð¶Ðµ ÑÐ»Ð¾Ð²Ð¾ Ñ Ð¿ÑÐ°Ð²Ð¸Ð»ÑÐ½ÑÐ¼ ÑÐµÐ³Ð¸ÑÑÑÐ¾Ð¼
			$keyword = $link->getKeyword();
			$maxReplacements = -1;
			if ($link->getMaxReplacements() > 0) {
				$maxReplacements = $link->getMaxReplacements();
			}
			$direction = 0;
			switch ($link->getOccurence()){
				case Mirasvit_SeoAutolink_Model_Config_Source_Occurence::FIRST:
					$direction = 0;
					break;
				case Mirasvit_SeoAutolink_Model_Config_Source_Occurence::LAST:
					$direction = 1;
					break;
				case Mirasvit_SeoAutolink_Model_Config_Source_Occurence::RANDOM:
					$direction = rand(0,1);
					break;
			}
			if ($direction == 1) {
				$text = strrev($text);
				$html = strrev($html);
				$keyword = strrev($keyword);
			}  
			//~ $pattern  = '#\b(' . str_replace('#', '\#', $keyword) . ')\b#iu';
			//~ $tokenized_text = preg_replace("{$pattern}", $html, $tokenized_text, $maxReplacements);
			$text = $this->replace($keyword, $html, $text, $maxReplacements);

			if ($direction == 1) {
				$text = strrev($text);
				$html = strrev($html);
				$keyword = strrev($keyword);
			} 		
		}
   		Varien_Profiler::stop('seoautolink_addLinks');
		return $text;
	}
	//replace words and left the same cases
	protected function replace($find, $replace, $source, $maxReplacements) {
		// substitute some special html characters with their 'real' value 
		$searchT = array('&Eacute;', 
		                  '&Euml;', 
		                  '&Oacute;', 
		                  '&eacute;', 
		                  '&euml;', 
		                  '&oacute;', 
		                  '&Agrave;', 
		                  '&Egrave;', 
		                  '&Igrave;', 
		                  '&Iacute;', 
		                  '&Iuml;', 
		                  '&Ograve;', 
		                  '&Ugrave;', 
		                  '&agrave;', 
		                  '&egrave;', 
		                  '&igrave;', 
		                  '&iacute;', 
		                  '&iuml;', 
		                  '&ograve;', 
		                  '&ugrave;', 
		                  '&Ccedil;', 
		                  '&ccedil;' 
		                 ); 
		$replaceT = array('Ã', 
		                   'Ã', 
		                   'Ã', 
		                   'Ã©', 
		                   'Ã«', 
		                   'Ã³', 
		                   'Ã', 
		                   'Ã', 
		                   'Ã', 
		                   'Ã', 
		                   'Ã', 
		                   'Ã', 
		                   'Ã', 
		                   'Ã ', 
		                   'Ã¨', 
		                   'Ã¬', 
		                   'Ã­', 
		                   'Ã¯', 
		                   'Ã²', 
		                   'Ã¹', 
		                   'Ã', 
		                   'Ã§' 
		                  ); 
		$source = str_replace($searchT, $replaceT, $source); 
		   
		  // matches for these expressions will be replaced with a unique placeholder 
		$preg_patterns = array( 
		      '#<!--.*?-->#s'       // html comments 
		    , '#<a[^>]*>.*?</a>#si' // html links 
		    , '#<[^>]+>#'           // generic html tag 
		    //~ , '#&[^;]+;#'           // special html characters 
		    //~ , '#[^ÃÃÃÃ©Ã«Ã³ÃÃÃÃÃÃÃÃ Ã¨Ã¬Ã­Ã¯Ã²Ã¹ÃÃ§\w\s]+#'   // all non alfanumeric characters, spaces and some special characters 
		); 

		$pl = new Mirasvit_TextPlaceholder($source, $preg_patterns); 
		// raw text, void of any html. 
		$source = $pl->get_tokenized_text(); 

		if ($maxReplacements == -1) {
			$maxReplacements = 9999999;
		}
		$pos = 0;
		for ($i=0; $i< $maxReplacements; $i++) {
			if (extension_loaded('mbstring')) {
				//standard function doesnt work with russian letters
				$pos = strpos(mb_convert_case($source, MB_CASE_LOWER, "UTF-8"), mb_convert_case($find, MB_CASE_LOWER, "UTF-8"), $pos);
			} else {
				$pos = stripos($source, $find, $pos);
			}
			if ($pos === false) {
				break;
			} else {
				if (isset($source[$pos-1]) && ctype_alnum($source[$pos-1])) {
					$pos += strlen($find);
					continue;
				}
				if (isset($source[$pos+strlen($find)]) && ctype_alnum($source[$pos+strlen($find)])) {
					$pos += strlen($find);
					continue;
				}				
			}

			$found = substr($source, $pos, strlen($find));
			//ÑÐ¾Ð·Ð´Ð°ÐµÐ¼ ÑÑÑÐ¾ÐºÑ, Ð½Ð° ÐºÐ¾ÑÐ¾ÑÑÑ Ð¼Ñ Ð±ÑÐ´ÐµÐ¼ Ð² Ð´Ð°Ð»ÑÐ½ÐµÐ¹ÑÐµÐ¼ Ð¼ÐµÐ½ÑÑÑ ÑÐ»Ð¾Ð²Ð°, Ñ Ð¿ÑÐ°Ð²Ð¸Ð»ÑÐ½ÑÐ¼ ÑÐµÐ³Ð¸ÑÑÑÐ¾Ð¼ ÑÐ»Ð¾Ð²;
			$replaceWithCorrectCase =  str_replace('----->'.$find, $found, $replace); //-----> Ð¼Ñ Ð²ÑÑÑÐ²Ð»ÑÐ»Ð¸ ÑÐ°Ð½Ð½ÐµÐµ Ð¿ÑÐ¸ ÑÐ¾ÑÐ¼Ð¸ÑÐ¾Ð²Ð°Ð½Ð¸Ð¸ ÑÑÑÐ»ÐºÐ¸ Ð´Ð»Ñ ÑÑÐ¾Ð³Ð¾ ÑÐ»ÑÑÐ°Ñ
			//Ð·Ð°Ð¼ÐµÐ½ÑÐµÐ¼ ÑÐ»Ð¾Ð²Ð° Ð² Ð¸ÑÑÐ¾Ð´Ð½Ð¾Ð¼ ÑÐµÐºÑÑÐµ
			$source = substr_replace($source, $replaceWithCorrectCase, $pos, strlen($find));
			$pos += strlen($replaceWithCorrectCase);
		}

		// we will later need this to put the html we stripped out, back in. 
		$translation_table = $pl->get_translation_table(); 
		// reconstruct the original text (now with links)   
		foreach ($translation_table as $key=>$value)  
		{
			$source = str_replace($key, $value, $source); 
		} 
		// replace the special html characters 
		$source = str_replace($replaceT, $searchT, $source); 		
		return $source;
	}
}

class Mirasvit_TextPlaceholder { 

    var $_translation_table = array(); 

    function __construct($text, $patterns) { 
        $this->_tokenized_text = preg_replace_callback($patterns, array($this, 'placeholder'), $text); 
    } 
     
    function get_translation_table() { 
        return $this->_translation_table; 
    } 
     
    function get_tokenized_text() { 
        return $this->_tokenized_text; 
    } 
     
    function placeholder($matches) {
        $sequence = count($this->_translation_table); 
        $placeholder = ' xkjndsfkjnakcx' . $sequence . 'cxmkmweof329jc '; 
        $this->_translation_table[$placeholder] = $matches[0]; 
        return $placeholder; 
    } 

}  
