<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Advanced SEO Suite
 * @version   1.0.3
 * @revision  279
 * @copyright Copyright (C) 2013 Mirasvit (http://mirasvit.com/)
 */


class Mirasvit_Seo_Model_Opengraph extends Mage_Core_Model_Abstract
{
    public function getConfig()
    {
        return Mage::getSingleton('seo/config');
    }

    public function modifyHtmlResponse($e)
    {
        if (Mage::getSingleton('admin/session')->getUser()) {
            return;
        }

        $response = $e->getResponse();
        $config   = $this->getConfig();
        $str      = '';

        if ($config->isRichSnippetsEnabled()) {
            $str .=' prefix="og: http://ogp.me/ns#" ';
        }

        if ($config->isOpenGraphEnabled()) {
            $str .=' xmlns:fb="http://www.facebook.com/2008/fbml" ';
        }

        if ($str == '') {
            return;
        }

        $body = $response->getBody();
        if (strpos($body, '<!DOCTYPE html') !== 0 && strpos($body, '<html') !== 0) {
            return;
        }

        $label = "<!-- mirasvit open graph begin -->";
        if (strpos($body, $label) !== false) {
            return;
        }

        $body = str_replace('<html', '<html'.$str, $body);
        if ($product = Mage::registry('current_product')) {
            //$product = Mage::getModel('catalog/product')->load($product->getId());
            $tags   = array();
            $tags[] = $label;
            $tags[] = $this->createMetaTag('title', $product->getName());
            
            $tags[] = $this->createMetaTag('description', $product->getShortDescription());
            $tags[] = $this->createMetaTag('type', 'product');
            $tags[] = $this->createMetaTag('url', $product->getProductUrl());

            if ($product->getImage()!='no_selection') {
                $tags[] = $this->createMetaTag('image', Mage::helper('catalog/image')->init($product, 'image'));
            }

            foreach($product->getMediaGalleryImages() as $image) {                
                if ($image->getFile()) {
                    $tags[] = $this->createMetaTag('image', Mage::helper('catalog/image')->init($product, 'image', $image->getFile()));
                }
            }

            $tags[] = "<!-- mirasvit open graph end -->";
            $body   = str_replace('<head>', "<head>\n".implode($tags, "\n"), $body);
        }

        $response->setBody($body);
    }

    protected function createMetaTag($property, $value)
    {
        $value = Mage::helper('seo')->cleanMetaTag($value);
        
        return "<meta property=\"og:$property\" content=\"$value\"/>";
    }
}
