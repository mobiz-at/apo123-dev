<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Advanced SEO Suite
 * @version   1.0.3
 * @revision  279
 * @copyright Copyright (C) 2013 Mirasvit (http://mirasvit.com/)
 */


class Mirasvit_SEO_Test_Helper_ParseTest extends PHPUnit_Framework_TestCase 
{
    public function setUp() {
        parent::setUp();
        $this->parseHelper = Mage::helper('seo/parse');
        //we load product from collection, because we need to load its attributes
        $collection = Mage::getModel('catalog/product')->getCollection()
        				->addFieldToFilter('entity_id', 166)
        				->addAttributeToSelect('*')
        				;
        $product = $collection->getFirstItem(); //http://devstore.dva/electronics/cell-phones/htc-touch-diamond.html
    	$category = Mage::getModel('catalog/category')->load(8); //http://devstore.dva/electronics/cell-phones.html
    	$store = Mage::getModel('core/store')->load(1);

    	$product->setName("HTC Touch Diamond [ru, uk] {ru, uk}");

    	$this->objects = array(
    		'product'=>$product,
    		'category'=>$category,
    		'store'=>$store,
		);

    }

	/**
	* @dataProvider parseProvider 
	*/
    public function testParse($template, $expectedResult) {
    	$result = $this->parseHelper->parse($template, $this->objects);

        $this->assertequals($expectedResult, $result);
    }

    public function parseProvider()
    {
        return array(
          array('[product_name][, model: {product_model}]', 'HTC Touch Diamond [ru, uk] {ru, uk}, model: HTC Touch Diamond'),
          array('[product_name][, model: {product_unknown}]', 'HTC Touch Diamond [ru, uk] {ru, uk}'),
          array('[product_name][product_unknown]', 'HTC Touch Diamond [ru, uk] {ru, uk}'),
          array('price [product_price]', 'price $750.00'),
        );
    }    
 
}
