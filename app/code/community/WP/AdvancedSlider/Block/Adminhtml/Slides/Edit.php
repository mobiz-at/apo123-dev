<?php

class WP_AdvancedSlider_Block_Adminhtml_Slides_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'advancedslider';
        $this->_controller = 'adminhtml_slides';

        $this->_updateButton('back', 'label', Mage::helper('adminhtml')->__('Back to Edit Slider'));
        $this->_updateButton('save', 'label', Mage::helper('adminhtml')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('adminhtml')->__('Delete'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        if ($sliderId = Mage::registry('advancedslider_slide_data')->getSliderId())
        {
            $this->_addButton('slider_preview', array(
                'label'     => Mage::helper('adminhtml')->__('Preview'),
                'onclick'   => Mage::helper('advancedslider')->getSliderPreviewLink($sliderId),
                'title'     => Mage::helper('advancedslider')->__('Slider Preview'),
            ), -101);

            $this->_addButton('add_slide', array(
                'label'     => Mage::helper('advancedslider')->__('Add Slide'),
                'onclick'   => 'setLocation(\'' . Mage::helper('adminhtml')->getUrl('advancedslider/slides/new', array('slider' => $sliderId)) . '\');',
            ), -102);
        }

        $this->_formScripts[] = "
            function toggleEditor()
            {
                if (tinyMCE.getInstanceById('advancedslider_slide_html') == null)
                {
                    tinyMCE.execCommand('mceAddControl', false, 'advancedslider_slide_html');
                }
                else
                {
                    tinyMCE.execCommand('mceRemoveControl', false, 'advancedslider_slide_html');
                }
            }

            function saveAndContinueEdit()
            {
                editForm.submit($('edit_form').action + 'back/edit/' + getActiveTabName());
            }

            function getActiveTabName()
            {
                if ($('advancedslider_slide_tabs_general').hasClassName('active')) return 'tab/general';
                if ($('advancedslider_slide_tabs_advanced').hasClassName('active')) return 'tab/advanced';
                if ($('advancedslider_slide_tabs_activity').hasClassName('active')) return 'tab/activity';
                return '';
            }
        ";
    }

    public function getHeaderText()
    {
        $styleId    = Mage::registry('slider')->getStyle();
        $styles     = Mage::getSingleton('advancedslider/source_style')->getOptionArray();
        $style      = isset($styles[$styleId]) ? $this->htmlEscape($styles[$styleId]) : '';

        if (Mage::registry('advancedslider_slide_data') && Mage::registry('advancedslider_slide_data')->getId())
        {
            return $this->__("Edit Slide '%s' (%s)", $this->htmlEscape(Mage::registry('advancedslider_slide_data')->getTitle()), $style);
        }
        else
        {
            return $this->__('New Slide (%s)', $style);
        }
    }

    public function getButtonsHtml($area = null)
    {
        $out        = parent::getButtonsHtml($area);
        $sliderId   = Mage::registry('advancedslider_slide_data')->getSliderId();
        $slideId    = Mage::registry('advancedslider_slide_data')->getSlideId();

        $collection = Mage::getModel('advancedslider/slides')->getCollection()
            ->addFieldToFilter('slider_id', array('eq' => $sliderId))
            ->addFieldToFilter('slide_id', array('neq' => $slideId))
            ->setOrder('sort_order', 'asc');

        $options    = array(0 => $this->__('--Go to a Slide--'));
        foreach ($collection as $item)
            $options[$item->getId()] = $item->getId() . ' / ' . $item->getTitle();

        if (count($options) == 1) return $out;

        $select = Mage::app()->getLayout()->createBlock('core/html_select')
            ->setName('goToSlide')
            ->setId('goToSlide')
            ->setTitle($this->__('Go to a Slide'))
            ->setValue(0)
            ->setOptions($options)
            ->setExtraParams('onchange="setLocation(\'' . Mage::helper('adminhtml')->getUrl('advancedslider/slides/edit', array('id' => '{{id}}')) . '\'.replace(\'{{id}}\', this.options[this.selectedIndex].value));"');
        return $select->getHtml() . $out;
    }

    public function getBackUrl()
    {
        $id = Mage::registry('advancedslider_slide_data')->getSliderId();
        return Mage::helper('adminhtml')->getUrl('advancedslider/sliders/edit', array('id' => $id, 'tab' => 'slides'));
    }
}
