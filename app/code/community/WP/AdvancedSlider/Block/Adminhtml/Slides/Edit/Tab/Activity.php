<?php

class WP_AdvancedSlider_Block_Adminhtml_Slides_Edit_Tab_Activity extends Mage_Adminhtml_Block_Widget_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('advancedslider/slide_activity.phtml');
    }

    protected function _prepareLayout()
    {
        $this->setChild('activity_options', $this->getLayout()->createBlock('advancedslider/adminhtml_slides_edit_tab_activity_options'));

        return parent::_prepareLayout();
    }
}
