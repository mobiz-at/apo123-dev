<?php

class WP_AdvancedSlider_Model_Source_Style extends Varien_Object
{
    const STYLE_STANDARD    = 'standard';
    const STYLE_NICOLE      = 'nicole';
    const STYLE_KRISTA      = 'krista';
    const STYLE_XANDRA      = 'xandra';
    const STYLE_TRISHA      = 'trisha';
    const STYLE_SAMANTA     = 'samanta';
    const STYLE_AMANDA      = 'amanda';

    public static function getOptionArray()
    {
        return array(
            self::STYLE_STANDARD    => Mage::helper('advancedslider')->__('Standard'),
            self::STYLE_KRISTA      => Mage::helper('advancedslider')->__('Krista'),
            self::STYLE_NICOLE      => Mage::helper('advancedslider')->__('Nicole'),
            self::STYLE_XANDRA      => Mage::helper('advancedslider')->__('Xandra'),
            self::STYLE_TRISHA      => Mage::helper('advancedslider')->__('Trisha'),
            self::STYLE_SAMANTA     => Mage::helper('advancedslider')->__('Samantha'),
            self::STYLE_AMANDA      => Mage::helper('advancedslider')->__('Amanda'),
        );
    }
}
