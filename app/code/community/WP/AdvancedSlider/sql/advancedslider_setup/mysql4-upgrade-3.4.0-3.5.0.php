<?php

$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE {$installer->getTable('advancedslider_sliders')} ADD `full_width` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `height`;

    ");

$installer->endSetup();
