<?php

$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE {$installer->getTable('advancedslider_sliders')}
    CHANGE `width` `width` VARCHAR( 10 ) NOT NULL DEFAULT '500';

    ");

$installer->endSetup();
