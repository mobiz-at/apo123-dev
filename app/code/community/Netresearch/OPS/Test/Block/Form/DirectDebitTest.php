<?php

class Netresearch_OPS_Test_Block_Form_DirectDebitTest extends EcomDev_PHPUnit_Test_Case
{
    public function testTemplate()
    {
        //Frontend case
        $modelMock = $this->getModelMock('ops/config', array('isFrontendEnvironment'));
        $modelMock->expects($this->any())
            ->method('isFrontendEnvironment')
            ->will($this->returnValue(true));
        $this->replaceByMock('model', 'ops/config', $modelMock);
        $ccForm = new Netresearch_OPS_Block_Form_DirectDebit();
        $this->assertEquals(Netresearch_OPS_Block_Form::FRONTEND_TEMPLATE, $ccForm->getTemplate());

        //Backend case
        $modelMock = $this->getModelMock('ops/config', array('isFrontendEnvironment'));
        $modelMock->expects($this->any())
            ->method('isFrontendEnvironment')
            ->will($this->returnValue(false));
        $this->replaceByMock('model', 'ops/config', $modelMock);
        $ccForm = new Netresearch_OPS_Block_Form_DirectDebit();
        $this->assertEquals(Netresearch_OPS_Block_Form_DirectDebit::BACKEND_TEMPLATE, $ccForm->getTemplate());
    }
}
