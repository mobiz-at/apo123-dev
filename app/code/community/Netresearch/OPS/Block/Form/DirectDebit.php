<?php
/**
 * Netresearch_OPS_Block_Form_DirectDebit
 *
 * @package   OPS
 * @copyright 2012 Netresearch App Factory AG <http://www.netresearch.de>
 * @author    Thomas Birke <thomas.birke@netresearch.de>
 * @license   OSL 3.0
 */
class Netresearch_OPS_Block_Form_DirectDebit extends Netresearch_OPS_Block_Form
{
    /**
     * Backend Payment Template
     */
    const BACKEND_TEMPLATE = 'ops/form/directDebit.phtml';
    
    protected function _construct()
    {
        parent::_construct();

        //Only in case that the form is loaded in the backend, use a special template
        if (false === Mage::getModel("ops/config")->isFrontendEnvironment()) {
            $this->setTemplate(self::BACKEND_TEMPLATE);
        }
    }

    /**
     * get ids of supported countries
     *
     * @return array
     */
    public function getDirectDebitCountryIds()
    {
        return explode(',', $this->getConfig()->getDirectDebitCountryIds());
    }
}
