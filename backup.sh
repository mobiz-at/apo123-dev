#!/bin/bash
php maintenance.php clean=log
php maintenance.php clean=var
php n98-magerun.phar db:dump --quiet --force --compression="gzip" backup-`date +%Y%m%d`-magento_dev.sql.gz
zip backup-`date +%Y%m%d`-magento_dev.zip -r -q *
cp backup-`date +%Y%m%d`-magento_dev.zip /var/www/backup
rm backup-`date +%Y%m%d`-magento_dev.zip
rm backup-`date +%Y%m%d`-magento_dev.sql.gz
