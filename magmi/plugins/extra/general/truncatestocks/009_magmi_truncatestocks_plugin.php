<?php

class Magmi_TruncateStocksPlugin extends Magmi_GeneralImportPlugin
{
    protected $_active;

    public function __construct()
    {
        $_active = false;
    }

    public function getPluginInfo()
    {
        return array("name"=>"Truncate_Stocks","author"=>"Meeeeee for apo123.at for deleting all stocks before a stock update","version"=>"0.0.1",
            "sponsorinfo"=>array("name"=>" ","url"=>"http://www.apo123.at"),
            "url"=>"");
    }

    public function initialize($params)
    {
        $mbdir = Magmi_Config::getInstance()->get('MAGENTO', 'basedir');
    }


    public function beforeImport()
    {
        $this->log("BEGIN truncate stock qty for all products in shop","info");

        define('MAGENTODIR', realpath(MAGMI_BASEDIR."/.."));
        require_once MAGENTODIR . '/app/Mage.php';
        umask(0);
        #error_log(  MAGENTODIR ,3,'/var/www/dev/var/log/system.log');
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $resource = Mage::getSingleton('core/resource');
        $tblStockItem = $resource->getTableName('cataloginventory/stock_item');
        $tblStockStatus = $resource->getTableName('cataloginventory/stock_status');
        $this->update("UPDATE {$tblStockItem} SET qty=0");
        $this->update("UPDATE {$tblStockItem} SET is_in_stock=0");
        $this->update("UPDATE {$tblStockStatus} SET qty=0");

        $this->log("END   truncate stock qty for all products in shop","info");
    }

    public function getPluginParamNames()
    {
        return array("MRAGENT:baseurl");
    }
}
?>